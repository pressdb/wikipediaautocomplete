import Loader from 'lodash'
import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { Dropdown } from 'semantic-ui-react'
import axios from 'axios'


/*TODO: 
1. Is updated search being overwritten? (Ex. Try typing "The Guardian")
Due to asynchronicity?
2. "(disambiguation)" pages are showing up.
3. Refactor code from "handleChange" and "handleSearchChange"
4. How to enable Dynamic Options (from https://dash.plot.ly/dash-core-components/dropdown),
leaving "Type one or more keywords here..." as the placeholder.
*/


function isEmpty(str) {
  return (!str || 0 === str.length);
}

function wikiParse(wikiresponse) {
  var response = {results : []}
  if(!wikiresponse) {
    return
  }
  if(wikiresponse === undefined){
    return
  }
  var wikipages = wikiresponse[1]
  if(wikipages === undefined){
    return
  }
  for(var wikipage of wikipages){
    response.results.push({
      name: wikipage,
      text: wikipage,
      value: wikipage
    })
  }
  //console.log(response)
  return response.results
}


export default class WikipediaAutoComplete extends Component {
  constructor(props) {
  super(props)
  this.state = { searchQuery: ' ',
            loaded: false,
          value: [],
          options: [] }

  }

  componentDidMount() {
    var url = "https://en.wikipedia.org/w/api.php?origin=*&action=opensearch&search="+this.state.searchQuery+'&limit=10&namespace=0&format=json'
    axios.get(url)
      .then(response => {
        if(!isEmpty(this.state.searchQuery)){
          this.setState({ options: wikiParse(response.data), 
          loaded: true })
        }
      }) 
  }

  handleChange = (e, { searchQuery, value }) => {
    this.setState({ searchQuery, value })
    var url = "https://en.wikipedia.org/w/api.php?origin=*&action=opensearch&search="+this.state.searchQuery+'&limit=10&namespace=0&format=json'
    axios.get(url)
      .then(response => {
      var newWikiResults = wikiParse(response.data)
    var allWikiResults = this.state.options
    var allWikiKeys = []
    for(const result of allWikiResults){
      allWikiKeys.push(result.name)
    }
    console.log(allWikiKeys)
    if(newWikiResults === undefined){
      this.setState({ searchQuery: searchQuery,
        value: value,
        options: allWikiResults })
    } else {
    for(const newWikiResult of newWikiResults){
      if(allWikiKeys.includes(newWikiResult.name) === false){
        allWikiResults.push(newWikiResult)
      }
    }
            this.setState({ searchQuery: searchQuery,
            value: value,
            options: allWikiResults })
            
            this.props.setProps({value})
            }
      }) 
    
  }


  handleSearchChange = (e, { searchQuery }) => {
    this.setState({ searchQuery })
    var url = "https://en.wikipedia.org/w/api.php?origin=*&action=opensearch&search="+this.state.searchQuery+'&limit=10&namespace=0&format=json'
    axios.get(url)
      .then(response => {
      var newWikiResults = wikiParse(response.data)
      var allWikiResults = this.state.options
      var allWikiKeys = []
      for(const result of allWikiResults){
        allWikiKeys.push(result.name)
      }
      console.log(allWikiKeys)
      if(newWikiResults === undefined){
        this.setState({ searchQuery: searchQuery,
          options: allWikiResults })
      } else{
      for(const newWikiResult of newWikiResults){
        if(allWikiKeys.includes(newWikiResult.name) === false){
          allWikiResults.push(newWikiResult)
        }
      }
      this.setState({ searchQuery: searchQuery,
        options: allWikiResults })

      }
        })
      
  }

  render() {
    const { setProps } = this.props
    var { loaded, options, searchQuery, value } = this.state
    if(!loaded) return <Loader />

    if(!value) value = []
    //console.log(searchQuery)
    //console.log(options)
    //console.log(value)
    return (
      <Dropdown
        clearable
        fluid
        //loading
        multiple
        minCharacters={2}
        onChange={this.handleChange}
        onSearchChange={this.handleSearchChange}
        options={options}
        placeholder="Type one or more keywords here..."
        search
        searchQuery={searchQuery}
        selection
        value={value}
      />
    )
  }
}





WikipediaAutoComplete.propTypes = {
    /**
     * The ID used to identify this component in Dash callbacks.
     */
    id: PropTypes.string,

    /**
     * An array of options {label: [string|number], value: [string|number]},
     * an optional disabled field can be used for each option
     */
    options: PropTypes.arrayOf(
        PropTypes.exact({
            /**
             * The dropdown's label
             */
            name: PropTypes.oneOfType([PropTypes.string, PropTypes.number])
                .isRequired,

            text: PropTypes.oneOfType([PropTypes.string, PropTypes.number])
                .isRequired,
            /**
             * The value of the dropdown. This value
             * corresponds to the items specified in the
             * `value` property.
             */
            value: PropTypes.oneOfType([PropTypes.string, PropTypes.number])
                .isRequired,

            /**
             * If true, this option is disabled and cannot be selected.
             */
            disabled: PropTypes.bool,
        })
    ),

    /**
     * The value of the input. If `multi` is false (the default)
     * then value is just a string that corresponds to the values
     * provided in the `options` property. If `multi` is true, then
     * multiple values can be selected at once, and `value` is an
     * array of items with values corresponding to those in the
     * `options` prop.
     */
    value: PropTypes.oneOfType([
        PropTypes.string,
        PropTypes.number,
        PropTypes.arrayOf(
            PropTypes.oneOfType([PropTypes.string, PropTypes.number])
        ),
    ]),

    /**
     * height of each option. Can be increased when label lengths would wrap around
     */
    optionHeight: PropTypes.number,

    /**
     * className of the dropdown element
     */
    className: PropTypes.string,

    /**
     * Whether or not the dropdown is "clearable", that is, whether or
     * not a small "x" appears on the right of the dropdown that removes
     * the selected value.
     */
    clearable: PropTypes.bool,

    /**
     * If true, this dropdown is disabled and the selection cannot be changed.
     */
    disabled: PropTypes.bool,

    /**
     * If true, the user can select multiple values
     */
    multi: PropTypes.bool,

    /**
     * The grey, default text shown when no option is selected
     */
    placeholder: PropTypes.string,

    /**
     * Whether to enable the searching feature or not
     */
    searchable: PropTypes.bool,

    /**
     * The value typed in the DropDown for searching.
     */
    search_value: PropTypes.string,

    /**
     * Dash-assigned callback that gets fired when the input changes
     */
    setProps: PropTypes.func,

    /**
     * Defines CSS styles which will override styles previously set.
     */
    style: PropTypes.object,

    /**
     * Object that holds the loading state object coming from dash-renderer
     */
    loading_state: PropTypes.shape({
        /**
         * Determines if the component is loading or not
         */
        is_loading: PropTypes.bool,
        /**
         * Holds which property is loading
         */
        prop_name: PropTypes.string,
        /**
         * Holds the name of the component that is loading
         */
        component_name: PropTypes.string,
    }),

    /**
     * Used to allow user interactions in this component to be persisted when
     * the component - or the page - is refreshed. If `persisted` is truthy and
     * hasn't changed from its previous value, a `value` that the user has
     * changed while using the app will keep that change, as long as
     * the new `value` also matches what was given originally.
     * Used in conjunction with `persistence_type`.
     */
    persistence: PropTypes.oneOfType([
        PropTypes.bool,
        PropTypes.string,
        PropTypes.number,
    ]),

    /**
     * Properties whose user interactions will persist after refreshing the
     * component or the page. Since only `value` is allowed this prop can
     * normally be ignored.
     */
    persisted_props: PropTypes.arrayOf(PropTypes.oneOf(['value'])),

    /**
     * Where persisted user changes will be stored:
     * memory: only kept in memory, reset on page refresh.
     * local: window.localStorage, data is kept after the browser quit.
     * session: window.sessionStorage, data is cleared once the browser quit.
     */
    persistence_type: PropTypes.oneOf(['local', 'session', 'memory']),
}

WikipediaAutoComplete.defaultProps = {
    clearable: true,
    disabled: false,
    multi: true,
    searchable: true,
    optionHeight: 35,
    persisted_props: ['value'],
    persistence_type: 'local'
}

export const propTypes = WikipediaAutoComplete.propTypes
export const defaultProps = WikipediaAutoComplete.defaultProps