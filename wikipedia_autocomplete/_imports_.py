from .WikipediaAutocomplete import WikipediaAutocomplete

__all__ = [
    "WikipediaAutocomplete"
]