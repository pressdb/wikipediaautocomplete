# WikipediaAutocomplete

A [Dash](https://plotly.com/dash) component library for searching for and selecting one or more Wikipedia articles by article title in a dropdown menu, based on [Semantic UI React](https://react.semantic-ui.com/).

![GIF demo](img/wiki_autocomplete_demo.gif)


## Installation

### Python

From source:

```bash
git clone https://bitbucket.org/pressdb/wikipediaautocomplete.git
cd wikipediaautocomplete
python setup.py install
```

## Usage

```python
import dash
import dash_html_components as html

from wikipedia_autocomplete import WikipediaAutocomplete

external_stylesheets = [
    'https://cdn.jsdelivr.net/npm/semantic-ui@2.4.2/dist/semantic.min.css'
]
external_scripts = [
     "https://cdnjs.cloudflare.com/ajax/libs/lodash.js/4.15.0/lodash.min.js",
     "https://unpkg.com/semantic-ui-react/dist/umd/semantic-ui-react.min.js",
     "https://unpkg.com/axios/dist/axios.min.js",
     "https://cdn.neo4jlabs.com/neovis.js/v1.2.1/neovis.js"
]
app = dash.Dash(__name__, 
                external_scripts=external_scripts,
                external_stylesheets=external_stylesheets)
server = app.server

app.layout = html.Div([
    WikipediaAutocomplete(id = 'keywords')
])

if __name__ == '__main__':
    app.run_server(debug=True)

```
